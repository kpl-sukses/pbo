/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
8 Oktober 2020
Class for Testing
*/
class Test {
    public static void printMovablePoint (MovablePoint point){
        System.out.println(point);
    }

    public static void main(String[] args) {
        MovablePoint point1 = new MovablePoint (0,0);
        point1.moveUp();
        point1.moveUp();
        point1.moveDown();
        point1.moveRight();
        point1.moveRight();
        point1.moveRight();
        point1.moveLeft();
        System.out.println(point1); // Output must be : '2,1'
    }
}