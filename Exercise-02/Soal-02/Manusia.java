/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
*/

public class Manusia {
    public String nama;
    public int umur;

    public Manusia() {
        
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getNama() {
        return this.nama;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }
    public int getUmur() {
        return this.umur;
    }
}