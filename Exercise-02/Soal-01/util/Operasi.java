/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
22 September 2020
Object operasi
*/
package util;

public class Operasi {
    private float result;
    private String op;
    private int var1,var2;

    public Operasi() {

    }

    public void process(String op, int var1, int var2) {
        if(op.equals("+"))
            this.result = var1+var2;
        if(op.equals("-"))
            this.result = var1-var2;
        if(op.equals("*"))
            this.result = var1*var2;
        if(op.equals("/"))
            this.result = var1/var2;
    }
    public float getResult() {
        return this.result;
    }
}