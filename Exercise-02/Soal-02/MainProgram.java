/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
*/

import java.util.Scanner;

public class MainProgram {
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        Manusia[] mhs = new Manusia[5];

        for(int i=0; i<5; i++){
            mhs[i] = new Manusia();
            System.out.print("Nama : ");
            mhs[i].setNama(cin.nextLine());
            System.out.print("Umur : ");
            mhs[i].setUmur(Integer.parseInt(cin.nextLine()));
        }
        for(int i=0; i<5; i++){
            System.out.println("Orang ke " + (i+1));
            System.out.println("Nama : " + mhs[i].getNama());
            System.out.println("Umur : " + mhs[i].getUmur());
        }
    }
}