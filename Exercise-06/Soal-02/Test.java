/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
14 Oktober 2020
Class for Testing
*/
enum Buah {
    MANGGA(60,"A fresh enchanted mango which restores 100 mana."),
    APEL(50,"Bad apple, not the one Newton found falling on his head."),
    ANGGUR(40,"Red grape. Quite cheap but alchoholic."),
    JERUK(70,"The kind of orange which is not disturbing. These ones don't have face."),
    MELON(90,"Very big and beautiful melon, 36 in another measurement.");

    private int harga;
    private String desc;

    Buah(int harga, String desc){
        this.harga = harga;
        this.desc = desc;
    }
    public String getDesc(){
        return this.desc;
    }
    public int getHarga(){
        return this.harga;
    }
}
public class Test {
    public static void main(String[] args) {
        Buah buah[] = Buah.values();
        for(Buah fruit: buah) {
            System.out.println("This is " + fruit + ", " + fruit.getDesc() + " Costs " + fruit.getHarga() + " primos.\n");
        }
    }
}