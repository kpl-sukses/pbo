/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
29 September 2020
Second Child 2 on Exercise-03
*/
public class Human extends Alliance{
    private String paladin;
    private String everyman;
    private String horsemount;

    public Human(){
        
    }
    public void setPaladin(String paladin){
        this.paladin = paladin;
    }
    public String getPaladin(){
        return this.paladin;
    }
    public void setEveryMan(String everyman){
        this.everyman = everyman;
    }
    public String getEveryMan(){
        return this.everyman;
    }
    public void setHorseMount(String horsemount){
        this.horsemount = horsemount;
    }
    public String getHorseMount(){
        return this.horsemount;
    }
    public String toString(){
        return "Race : Human \nSpecialization : " + getPaladin() + "\nRacial Effect : " + getEveryMan() + "\nMount : " + getHorseMount();
    }
    @Override
    public void printName(String name){
        System.out.println("I am " + name + ", level " + getLevel() + ", Paladin of the Alliance.");
    }
}