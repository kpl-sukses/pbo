/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
*/

import java.util.Scanner;

public class Test {
    public static void main(String args[]) {
        Manusia mhs = new Manusia();

        Scanner cin = new Scanner(System.in);
        String input;
        int inputNpm;

        System.out.println("Masukkan nama : ");
        input = cin.nextLine();
        mhs.setNama(input);

        System.out.println("Masukkan npm : ");
        inputNpm = Integer.parseInt(cin.nextLine());
        mhs.setNpm(inputNpm);

        System.out.println("Nama saya " + mhs.getNama() + " dengan NPM " + mhs.getNpm());
    }
}