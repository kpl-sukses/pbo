/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
29 September 2020
First Child 2 on Exercise-03
*/
public class Alliance extends Mortal{
    private String king;

    public Alliance(){
        this.king = "Anduin Wrynn";
    }
    public String getKing(){
        return this.king;
    }
    public void printName(String name){
        System.out.println("I am " + name + " of the Alliance.");
    }
    public String toString(){
        return "I am " + getName() + " level " + getLevel() + " of the Alliance.";
    }
}