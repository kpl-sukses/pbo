/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
29 September 2020
Parent on Exercise-03
*/
public class Mortal{
    private String name;
    private String gender;
    private int level;

    public Mortal(){

    }
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return this.name;
    }
    public void setGender(String gender){
        this.gender = gender;
    }
    public String getGender(){
        return this.gender;
    }
    public void setLevel(int level){
        this.level = level;
    }
    public int getLevel(){
        return this.level;
    }
}