/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
*/

public class Manusia {
    public String nama;
    public int npm;

    public Manusia() {
        this.nama = "Nama";
        this.npm = 31;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    public String getNama() {
        return this.nama;
    }

    public void setNpm(int npm) {
        this.npm = npm;
    }
    public int getNpm() {
        return this.npm;
    }
}