/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
*/

public class Rekening {
    private int balance;

    public Rekening () {
        this.balance = 0;
    }
    public float getBalance() {
        return this.balance;
    }
    public void addBalance(int addValue) {
        this.balance += addValue;
    }
}