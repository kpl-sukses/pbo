/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
8 Oktober 2020
Interface movable
*/
public interface Movable {
    public void moveUp();
    public void moveDown();
    public void moveLeft();
    public void moveRight();
}