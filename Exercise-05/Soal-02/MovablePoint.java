/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
8 Oktober 2020
Class MovablePoint
*/
public class MovablePoint extends Movable {
    private int x,y;

    public MovablePoint(int x, int y){
        this.x = x;
        this.y = y;
    }
    public String toString(){
        return (this.x + "," + this.y);
    }
    @Override
    public void moveUp(){
        this.y += 1;
    }
    @Override
    public void moveDown(){
        this.y -= 1;
    }
    @Override
    public void moveLeft(){
        this.x -= 1;
    }
    @Override
    public void moveRight(){
        this.x += 1;
    }
}