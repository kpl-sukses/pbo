/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
22 September 2020
Main program Soal-01
*/
import java.util.Scanner;
import io.Portal;
import util.Operasi;

public class MainProgram {
    public static void main(String[] args) {
        Operasi operand = new Operasi();
        Portal worker = new Portal();

        worker.input();
        operand.process(worker.getOp(), worker.getVar1(), worker.getVar2());
        worker.output(operand.getResult());
    }
}