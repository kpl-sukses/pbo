/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
29 September 2020
Main program Exercise-03
*/
public class Test{
    public static void main(String[] args) {
        Orc warr = new Orc();
        Human pala = new Human();

        warr.setName("Crazyapril");
        warr.setGender("Male");
        warr.setLevel(80);
        warr.setWarrior("Fury");
        warr.setBloodFury("+124 Attack Power");
        warr.setWolfMount("Swift Horde Wolf");

        pala.setName("Nobbel");
        pala.setGender("Female");
        pala.setLevel(120);
        pala.setPaladin("Retribution");
        pala.setEveryMan("Remove sleep, charm, and fear effects.");
        pala.setHorseMount("Scarlet Deathcharger");


        warr.printName(warr.getName());
        System.out.println("Champion of the Banshee Queen, " + warr.getWarchief());
        System.out.println(warr.toString());

        System.out.println("");

        pala.printName(pala.getName());
        System.out.println("Champion of the King of Stormwind, " + pala.getKing());
        System.out.println(pala.toString());
    }
}