/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
*/
import java.util.Scanner;

public class MainProgram {
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        Rekening Account = new Rekening();
        int op, addValue;
        boolean stop=false;

        do{
            System.out.println("1. Show current balance 2. Add balance 0. Exit");
            System.out.print("Pick : ");
            op = Integer.parseInt(cin.nextLine());
            switch(op){
                case 1:
                System.out.println("\nCurrent Balance : " + Account.getBalance());
                break;
                case 2:
                System.out.print("\nPlease insert the amount to add : ");
                addValue = Integer.parseInt(cin.nextLine());
                Account.addBalance(addValue);
                break;
                case 0:
                stop = true;
                break;
                default:
                System.out.println("Input is not valid, please kindly re-input\n");
            }
        }while(stop==false);
    }
}