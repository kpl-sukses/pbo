/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
*/

import java.util.Scanner;

public class Output {
    public static void main(String args[]) {
        String kata;
        String kalimat = "";
        Scanner cin = new Scanner(System.in);

        System.out.println("Jumlah string yang akan diinput : ");
        int n = Integer.parseInt(cin.nextLine());

        for(int i=1; i<=n; i++){
            System.out.println("Input-" + i + " : ");
            kata = cin.nextLine();
            kalimat += kata;
        }
        System.out.println("");
        System.out.println(kalimat);
        
        cin.close();
    }
}