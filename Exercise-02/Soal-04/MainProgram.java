/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
*/
import java.util.Scanner;

public class MainProgram {
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);
        Operasi operand = new Operasi();
        int int1,int2;
        float float1,float2;

        int1 = Integer.parseInt(cin.nextLine()); 
        int2 = Integer.parseInt(cin.nextLine());
        float1 = Float.parseFloat(cin.nextLine());
        float2 = Float.parseFloat(cin.nextLine());

        System.out.println(operand.add(int1,int2));
        System.out.println(operand.add(float1,float2));
        cin.close();
    }
}