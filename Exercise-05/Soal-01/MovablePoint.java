/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
8 Oktober 2020
Class MovablePoint
*/
public class MovablePoint implements Movable {
    private int x,y;

    public MovablePoint(int x, int y){
        this.x = x;
        this.y = y;
    }
    public String toString(){
        return (this.x + "," + this.y);
    }
    public void moveUp(){
        this.y += 1;
    }
    public void moveDown(){
        this.y -= 1;
    }
    public void moveLeft(){
        this.x -= 1;
    }
    public void moveRight(){
        this.x += 1;
    }
}