/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
29 September 2020
Second Child 1 on Exercise-03
*/
public class Orc extends Horde{
    private String warrior;
    private String bloodfury;
    private String wolfmount;

    public Orc(){
        
    }
    public void setWarrior(String warrior){
        this.warrior = warrior;
    }
    public String getWarrior(){
        return this.warrior;
    }
    public void setBloodFury(String bloodfury){
        this.bloodfury = bloodfury;
    }
    public String getBloodFury(){
        return this.bloodfury;
    }
    public void setWolfMount(String wolfmount){
        this.wolfmount = wolfmount;
    }
    public String getWolfMount(){
        return this.wolfmount;
    }
    public String toString(){
        return "Race : Orc \nSpecialization : " + getWarrior() + "\nRacial Effect : " + getBloodFury() + "\nMount : " + getWolfMount();
    }
    @Override
    public void printName(String name){
        System.out.println("I am " + name + ", level " + getLevel() + ", Warrior of the Horde.");
    }
}