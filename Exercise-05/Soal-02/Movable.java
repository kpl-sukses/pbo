/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
8 Oktober 2020
Interface movable
*/
public abstract class Movable {
    public abstract void moveUp();
    public abstract void moveDown();
    public abstract void moveLeft();
    public abstract void moveRight();
}