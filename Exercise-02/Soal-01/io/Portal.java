/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
22 September 2020
Object operasi
*/
package io;

import java.util.Scanner;

public class Portal {
    Scanner cin = new Scanner(System.in);
    private String op;
    private int var1,var2;
    private float result;

    public Portal() {

    }

    public void input() {
        var1 = Integer.parseInt(cin.nextLine());
        var2 = Integer.parseInt(cin.nextLine());
        op = cin.nextLine();
        cin.close();
    }
    public int getVar1() {
        return this.var1;
    }
    public int getVar2() {
        return this.var2;
    }
    public String getOp() {
        return this.op;
    }
    public void output(float result) {
        System.out.println(result);
    }
}