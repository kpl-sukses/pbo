/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
14 Oktober 2020
Class for Testing
*/
import java.util.Scanner;

public class Konversi {
    static void Menu() {
        System.out.println("String-int-Integer Converter");
        System.out.println("Choose what you wanna input");
        System.out.println("Will automatically output the conversion");
        System.out.println("1. String \n2. Integer \n3. Primitive int");
        System.out.print("Input : ");
    }   
    public static void main(String[] args) {
        Scanner jin = new Scanner(System.in);
        int convint;
        Integer convInt;
        String convStr;

        Menu();
        int switcher = Integer.parseInt(jin.nextLine());
        switch (switcher) {
            case 1:
            System.out.println("Objek yang anda input adalah String.");
            String inputString = jin.nextLine();
            convint = Integer.parseInt(inputString);
            convInt = Integer.valueOf(inputString);
            System.out.println("Konversi ke int dengan nilai " + convint);
            System.out.println("Konversi ke Integer dengan nilai " + convInt + " dan identifikasi " + convInt.getClass());
            break;
            case 2:
            System.out.println("Objek yang anda input adalah Integer.");
            Integer inputInteger = Integer.valueOf(jin.nextLine());
            convStr = Integer.toString(inputInteger);
            convint = inputInteger.intValue();
            System.out.println("Konversi ke String dengan nilai " + convStr + " dan identifikasi " + convStr.getClass());
            System.out.println("Konversi ke int dengan nilai " + convint);
            break;
            case 3:
            System.out.println("Objek yang anda input adalah Primitive int.");
            int inputInt = Integer.parseInt(jin.nextLine());
            convInt = Integer.valueOf(inputInt);
            convStr = Integer.toString(inputInt);
            System.out.println("Konversi ke Integer dengan nilai " + convInt + " dan identifikasi " + convInt.getClass());
            System.out.println("Konversi ke String dengan nilai " + convStr + " dan identifikasi " + convStr.getClass());
            break;
            default:
            System.out.println("Input tidak dikenali, silakan mulai ulang program.");
        }
        jin.close();
    }
}