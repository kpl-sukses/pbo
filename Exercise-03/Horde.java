/*
Mochamad Arya Bima Agfian
140810190031
Kelas A
29 September 2020
First Child 1 on Exercise-03
*/
public class Horde extends Mortal{
    private String warchief;

    public Horde(){
        this.warchief = "Sylvanas Windrunner";
    }
    public String getWarchief(){
        return this.warchief;
    }
    public void printName(String name){
        System.out.println("I am " + name + " of the Horde.");
    }
    public String toString(){
        return "I am " + getName() + " level " + getLevel() + " of the Horde.";
    }
}